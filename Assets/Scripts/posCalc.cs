﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class posCalc : MonoBehaviour
{
    public double N;
    public double Nd;
    public double i;
    public double id;
    public double w;
    public double wd;
    public double a;
    public double e;
    public double ed;
    public double M;
    public double Md;

    public GameObject parent;

    private double w1, L, q, Q, P, T, v, E;
    private double d;

    private double radius;

    // Start is called before the first frame update
    void Start()
    {
        radius = gameObject.transform.localPosition.magnitude;
    }

    // Update is called once per frame
    void Update()
    {
        var N = getN();
        var i = geti();
        var w = getw();
        var e = gete();
        var M = getM();

        d = figureDate();
        w1 = N + w;
        L = M + w1;
        q = a * (1 - e);
        Q = a * (1 + e);
        P = Math.Pow(a, 1.5);
        // T =
        E = figureE(); // iterated function

        var xv = a * (Math.Cos(E) - e);
        var yv = a * (Math.Sqrt(1 - e * e) * Math.Sin(E));
        var v = Math.Atan2(yv, xv);
        var r = Math.Sqrt(xv * xv + yv * yv);

        var lon = v + w;

        // var xh = r * (Math.Cos(N) * Math.Cos(lon) - Math.Sin(N) * Math.Sin(lon) * Math.Cos(i));
        // var yh = r * (Math.Sin(N) * Math.Cos(lon) + Math.Cos(N) + Math.Sin(lon) * Math.Cos(i));
        // var zh = r * (Math.Sin(lon) * Math.Sin(i));

        gameObject.transform.localPosition = new Vector3((float)(radius * Math.Cos(lon)), 0, (float)(radius * Math.Sin(lon)));
    }

    double getN() { return coef(N, Nd) * Math.PI / 180; }
    double geti() { return coef(i, id) * Math.PI / 180; }
    double getw() { return coef(w, wd) * Math.PI / 180; }
    double gete() { return coef(e, ed); } // not an angle
    double getM() { return coef(M, Md) * Math.PI / 180; }

    double coef(double x0, double x1) { return x0 + x1 * d; }

    double figureDate()
    {
        var n = DateTime.Now;
        var jdday = 367 * n.Year - 7 * (n.Year + (n.Month + 9) / 12) / 4 + 275 * n.Month / 9 + n.Day - 730530;
        var jd = jdday + n.Hour / 24.0 + n.Minute / 60.0 / 24.0 + n.Second / 3600.0 / 24.0;
        return jd;
    }

    double figureE()
    {
        var M = getM();
        var e = gete();

        var E0 = M + e * Math.Sin(M) * (1 + e * Math.Cos(M));
        if (e < .05)
        {
            return E0;
        }
        int count = 0;
        while (count++ < 100)
        {
            var E1 = E0 - (E0 - e * Math.Sin(E0) - M) / (1 - e * Math.Cos(E0));
            if (Math.Abs(E0 - E1) < .001)
            {
                return E1;
            }
            E0 = E1;
        }
        return E0;
    }
}
